let visor = document.querySelector(".visor");
//COMO NECESITAREMOS UNA COLECCION DE BOTONES, UTILIZAREMOS GetElementByClassName
let botones = document.getElementsByClassName("boton");
console.log("Coleccion de botones ", botones);

for (let i = 0; i < botones.length; i++) {
    console.log("Agregamos el listerner a: ", botones[i]);
    botones[i].addEventListener("click", () => {
        console.log("El listener capturó el: ", botones[i], " y va a tomar acción");
        accion(botones[i]);
    });
}
function accion(boton) {
    console.log("valor del boton presionado", boton.innerHTML);
    switch (boton.innerHTML) {
        case 'C':
            console.log("Con C borra el visor");
            borrar();
            break;

        case '=':
            console.log("con =,calcula el resultado");
            resultado();
            break;

        default:
            console.log("Va armando el visor");
            actualizarVisor(boton);
            break;
    }
}
function borrar() {
    console.log("procedemos al borrado del visor");
    visor.innerHTML = "0";
}
function actualizarVisor(boton) {
    if (visor.innerHTML == 0) {
        visor.innerHTML = "";
    }
    visor.innerHTML += boton.innerHTML;
}

function resultado() {
    console.log("Intenta evaluar el contenido del visor");
    visor.innerHTML = eval(visor.innerHTML);      //INTENTA EVALUAR Y EJECUTAR LO QUE ESTA ESCRITO EN LO QUE LEE (visor.innerHTML)
}