document.querySelector("#login_btn").setAttribute("onclick", "validacion()");
let user = document.querySelector("#username");
let pass = document.querySelector("#password");
let mens = document.querySelector("#mensaje");

//...DOS VARIABLES PARA LA VERIFICACION CON UN SOLO USUARIO.....
//const nombre = "Eugenia";
//const password = "12345";

//.....ARRAY DE OBJETOS PARA LA VERIFICACION CON VARIOS USUARIOS.....
let usuarios_registrados = [];
usuarios_registrados[0] = { nombre: "Camila", password: "12345678" },
usuarios_registrados[1] = { nombre: "Ariel", password: "87654321" };
//....RECORRE LOS USUARIOS REGISTRADOS.....
for (let i = 0; i < usuarios_registrados.length; i++) {
    console.log(usuarios_registrados[i]);
}

function validacion() {
    mens.className = "rojo";
    //..........VERIFICION QUE NO ESTEN LOS INPUT VACIOS....... 
    if (user.value.trim().length === "0") {           
         //dummy
        mens.value = "FALTA NOMBRE DE USUARIOS";
        user.value = "";
        user.focus();
        return;
    }
    user.value = user.value.trim();

    if (pass.value.trim() === "") {     
        //dummy
        mens.value = "FALTA PASSWORD";
        pass.value = "";
        pass.focus();
        return;
    }

    //...VERIFICACION CON UN USUARIO......
    // if (nombre !== user.value || password !== pass.value) {
    // mens.value = "USUARIO INEXISTENTE";
    // user.focus();
    // user.value = "";
    //pass.value = "";
    //return;
    // }
   
    //...VERIFICACION CON VARIOS USUARIOS...........
     let encontroUser;
     encontroUser = false;
    for (let i = 0; i < usuarios_registrados.length; i++) {
        // || representa O en una condición mientras que && representa Y en una condición
 console.log("Estamos recorriendo el elemento", i,", es decir, el usuario registrado:" + usuarios_registrados[i].nombre);
        if (usuarios_registrados[i].nombre === user.value && usuarios_registrados[i].password === pass.value) {
            encontroUser = true;
            console.log("encontrado");
            break;
        }
    }
    if (encontroUser === false) {
        mens.value = "USUARIO INEXISTENTE";
        user.focus();
        user.value = "";
        pass.value = "";
        return;
    }


    mens.className = "verde";
    mens.value = "BIENVENIDO/A" + user.value + "...";

}